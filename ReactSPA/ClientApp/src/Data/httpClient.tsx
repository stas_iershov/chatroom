import { webAPIUrl } from './../AppSettings';

export interface HttpRequest<RequestData> {
  path: string;
  method?: string;
  body?: RequestData;
  accessToken?: string;
}
export interface HttpResponse<ResponseData> extends Response {
  parsedBody?: ResponseData;
}

export interface PostResult<T> {
  ok: boolean;
  error?: string;
  data?: T;
}

export const http = <RequestData, ResponseData>(
  reqData: HttpRequest<RequestData>,
): Promise<HttpResponse<ResponseData>> => {
  return new Promise((resolve, reject) => {
    const RequestData = new Request(`${webAPIUrl}${reqData.path}`, {
      method: reqData.method || 'get',
      headers: {
        'Content-Type': 'application/json',
      },
      body: reqData.body ? JSON.stringify(reqData.body) : undefined,
    });
    if (reqData.accessToken) {
      RequestData.headers.set('authorization', `bearer ${reqData.accessToken}`);
    }
    let response: HttpResponse<ResponseData>;
    fetch(RequestData)
      .then((res) => {
        console.log(res);
        response = res;
        let contentType = res.headers.get('Content-Type');
        if (contentType !== null && contentType.indexOf('json') > 0) {
          return res.json();
        } else {
          resolve(response);
        }
      })
      .then((body) => {
        if (response.ok) {
          response.parsedBody = body;
          resolve(response);
        } else {
          reject(body);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
};
