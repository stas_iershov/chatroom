import { http, PostResult } from './httpClient';

export interface MessageData {
  messageText: string;
  created: Date;
  messageId: number;
  userNickname: string;
}

export interface PostMessageData {
  messageText: string;
  roomId: number;
}

export const getMessages = async (token: string, roomId: number): Promise<MessageData[]> => {
  try {
    const result = await http<undefined, MessageData[]>({
      path: `/messages/${roomId}`,
      accessToken: token,
    });
    if (result.parsedBody) {
      return result.parsedBody;
    } else {
      return [];
    }
  } catch (ex) {
    return [];
  }
};

export const postMessage = async (token: string, messageData: PostMessageData): Promise<PostResult<void>> => {
  try {
    const result = await http<PostMessageData, undefined>({
      path: '/messages',
      accessToken: token,
      method: 'post',
      body: messageData,
    });

    if (result.ok) {
      return { ok: true };
    } else {
      return { ok: false, error: await 'Unexpected error occured' };
    }
  } catch (ex) {
    return { ok: false, error: `Unexpected error occured: ${ex.errorMessage}` };
  }
};
