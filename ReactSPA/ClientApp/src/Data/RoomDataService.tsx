import { http, PostResult } from './httpClient';

export interface RoomData {
  name: string;
  joined: boolean;
  numberOfUsers: number;
  id: number;
}

export interface PostRoomData {
  name: string;
  userNickname: string;
}

export interface PostJoinToRoomData {
  chatRoomId: number;
  userNickname: string;
}

export const createRoom = async (token: string, roomData: PostRoomData): Promise<PostResult<RoomData>> => {
  try {
    const result = await http<PostRoomData, RoomData>({
      path: '/rooms',
      accessToken: token,
      method: 'post',
      body: roomData,
    });

    if (result.ok) {
      return { ok: true, data: result.parsedBody };
    } else {
      return { ok: false, error: await 'Unexpected error occured' };
    }
  } catch (ex) {
    return { ok: false, error: `Unexpected error occured: ${ex.errorMessage}` };
  }
};

export const joinToRoom = async (token: string, roomData: PostJoinToRoomData): Promise<PostResult<void>> => {
  try {
    const result = await http<PostJoinToRoomData, undefined>({
      path: '/rooms/join',
      accessToken: token,
      method: 'post',
      body: roomData,
    });

    if (result.ok) {
      return { ok: true };
    } else {
      return { ok: false, error: await 'Unexpected error occured' };
    }
  } catch (ex) {
    return { ok: false, error: `Unexpected error occured: ${ex.errorMessage}` };
  }
};

export const getRooms = async (token: string): Promise<RoomData[]> => {
  try {
    const result = await http<undefined, RoomData[]>({
      path: '/rooms',
      accessToken: token,
    });
    if (result.parsedBody) {
      return result.parsedBody;
    } else {
      return [];
    }
  } catch (ex) {
    return [];
  }
};
