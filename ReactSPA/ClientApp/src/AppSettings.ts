export const server =
  process.env.REACT_APP_ENV === 'staging' ? 'https://chatroom-backend.azurewebsites.net' : 'https://localhost:44399';

export const webAPIUrl = `${server}/api`;

export const authSettings = {
  domain: 'chatroom-demo.eu.auth0.com',
  clientId: 'XwvcEUbIdS1YwS5BfCIOAtGeP3wPvRMN',
  redirectUri: window.location.origin + '/signin-callback',
  scope: 'openid profile email',
  audience: 'https://chatroom',
};
