import React, { FC } from 'react';
import './chatPage.css';
import { MessageData } from '../../Data/MessageDataService';

export const ChatMessage: FC<MessageData> = ({ userNickname, messageText, created }) => {
  return (
    <div className="message">
      <div className="message-data">
        <strong>{userNickname}</strong>
        <span className="message-time"> {new Date(created).toLocaleTimeString()} </span>
      </div>
      <p>{messageText} </p>
    </div>
  );
};
