import React, { FC, useState, useEffect, useRef } from 'react';
import { Page } from './../Shared/Page';
import './chatPage.css';
import { ChatMessage } from './ChatMessage';
import { MessageData, getMessages, postMessage } from '../../Data/MessageDataService';
import { useAuth0 } from '@auth0/auth0-react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { HubConnectionBuilder, HubConnectionState, HubConnection } from '@aspnet/signalr';
import { server } from '../../AppSettings';

interface RouteParams {
  roomId: string;
}

export const ChatPage: FC<RouteComponentProps<RouteParams>> = ({ match }) => {
  const [messages, setMessages] = useState<MessageData[]>([]);
  const [messagesLoading, setMessagesLoading] = useState(true);
  const [authNeeded, setAuthNeeded] = useState(false);

  const [message, setMessage] = useState<string>('');
  const [messageSending, setMessageSending] = useState(false);

  const { isAuthenticated, getAccessTokenSilently, isLoading } = useAuth0();

  const contentRef = useRef<HTMLDivElement>(null);

  let audio = new Audio('/notification.wav');

  const playNotification = () => {
    audio.play();
  };

  const scrollToBottom = () => {
    if (contentRef !== null && contentRef.current !== null) {
      console.log('scrollToBottom');
      contentRef.current.scrollTop = contentRef.current.scrollHeight;
    }
  };

  const setUpSignalRConnection = async (roomId: number) => {
    const connectionBuilder = new HubConnectionBuilder()
      .withUrl(`${server}/messageshub`)
      .withAutomaticReconnect()
      .build();

    connectionBuilder.on('Message', (serverMessage: string) => {
      console.log('Message', serverMessage);
    });

    connectionBuilder.on('MessagePosted', (messageData: MessageData) => {
      console.log(messageData);
      setMessages((msg) => [...msg, messageData]);
      playNotification();
      scrollToBottom();
    });

    async function start() {
      try {
        await connectionBuilder.start();
      } catch (err) {
        console.log(err);
      }
    }
    await start();

    if (connectionBuilder.state === HubConnectionState.Connected) {
      connectionBuilder.invoke('SubscribeToRoom', roomId).catch((err: Error) => {
        return console.error(err.toString());
      });
    }

    return connectionBuilder;
  };

  const cleanUpSignalRConnection = (rooomId: number, hubConnection: HubConnection) => {
    if (!hubConnection) {
      return;
    }
    if (hubConnection.state === HubConnectionState.Connected) {
      hubConnection
        .invoke('UnsubscribeFromRoom', rooomId)
        .then(() => {
          hubConnection.off('Message');
          hubConnection.off('MessagePosted');
          hubConnection.stop();
        })
        .catch((err: Error) => {
          return console.error(err.toString());
        });
    } else {
      hubConnection.off('Message');
      hubConnection.off('MessagePosted');
      hubConnection.stop();
    }
  };

  useEffect(() => {
    let cancelled = false;
    const doGetMessages = async (roomId: number) => {
      const token = await getAccessTokenSilently();
      const serverMessages = await getMessages(token, roomId);
      if (!cancelled) {
        setMessages(serverMessages);
        setMessagesLoading(false);
        scrollToBottom();
      }
    };

    let hubConnection: HubConnection;
    if (!isLoading && isAuthenticated) {
      if (match.params.roomId) {
        const roomId = Number(match.params.roomId);
        doGetMessages(roomId);
      }
    } else {
      if (!isLoading && !isAuthenticated) {
        setAuthNeeded(true);
      }
    }

    if (isAuthenticated && !isLoading) {
      console.log('setUpSignalRConnection');
      setUpSignalRConnection(Number(match.params.roomId)).then((con) => {
        hubConnection = con;

        console.log('con', con);
      });
    }

    return () => {
      cancelled = true;
      if (match.params.roomId) {
        const roomId = Number(match.params.roomId);
        if (hubConnection) {
          cleanUpSignalRConnection(roomId, hubConnection);
        }
      }
    };
  }, [match.params.roomId, isLoading]);

  const handlePostMessage = async () => {
    const token = await getAccessTokenSilently();
    if (!isLoading && isAuthenticated) {
      if (match.params.roomId) {
        if (message.trim().length < 1) {
          return;
        }

        setMessageSending(true);
        const roomId = Number(match.params.roomId);
        const sendMessageResult = await postMessage(token, {
          messageText: message,
          roomId: roomId,
        });

        console.log('sendMessageResult:');
        console.log(sendMessageResult);

        if (sendMessageResult.ok) {
          setMessage('');
        } else {
          alert("Couldn't send the message");
        }

        setMessageSending(false);
      }
    } else {
      if (!isLoading && !isAuthenticated) {
        setAuthNeeded(true);
      }
    }
  };

  const handleScroll = async (event: React.UIEvent<HTMLDivElement, UIEvent>) => {
    //TODO: implement paging here
  };

  const handleMessageInput = async (event: React.FormEvent<HTMLInputElement>) => {
    setMessage(event.currentTarget.value);
  };

  const handleKeyDown = async (event: { key: string }) => {
    if (event.key === 'Enter') {
      handlePostMessage();
    }
  };

  return (
    <Page title="Chat">
      <div className="chat-page">
        <div ref={contentRef} onScroll={handleScroll} className="chat-window">
          {!messagesLoading &&
            messages &&
            messages.map((message) => <ChatMessage key={message.messageId} {...message}></ChatMessage>)}
          {messagesLoading && !authNeeded && <span>Loading...</span>}

          {!messagesLoading && messages.length === 0 && <span className="no-messages-text">No messages yet</span>}

          {authNeeded && (
            <div>
              <span>Cannot get messages. Try </span>
              <Link to="/signin">signing in</Link>
            </div>
          )}
        </div>

        <div className="chat-input">
          <input value={message} onChange={handleMessageInput} onKeyDown={handleKeyDown} type="text" />
          <input disabled={messagesLoading || messageSending} onClick={handlePostMessage} type="button" value="Send" />
        </div>
      </div>
    </Page>
  );
};
