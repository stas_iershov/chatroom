import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './header.css';
import { useAuth0 } from '@auth0/auth0-react';

const Header = () => {
  const { user, isAuthenticated } = useAuth0();
  const [navActive, setNavActive] = useState(false);

  const collapseNav = async () => {
    setNavActive(!navActive);
  };

  return (
    <div>
      <header>
        <Link to="/">
          <h1 className="logo">CHATROOM</h1>
        </Link>
        <ul className={navActive ? 'nav-active nav' : 'nav'}>
          <li className="nav-link">
            <Link onClick={collapseNav} to="/rooms">
              <span>Rooms</span>
            </Link>
          </li>
          <li className="nav-link">
            <Link onClick={collapseNav} to="/create">
              <span>Create</span>
            </Link>
          </li>
          <li className="nav-link"></li>
        </ul>

        {!isAuthenticated && (
          <Link to="/signin">
            <span>Sign in</span>
          </Link>
        )}

        {isAuthenticated && (
          <div>
            <span className="user-block-item">Hello, {user.nickname} </span>
            <Link className="user-block-item" to="/signout">
              <span>Sign out</span>
            </Link>
          </div>
        )}

        <div onClick={collapseNav} className="burger">
          <i className="fas fa-bars"></i>
        </div>
      </header>
    </div>
  );
};

export default Header;
