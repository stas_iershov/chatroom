import React from 'react';
import { Link } from 'react-router-dom';

const HomePage = () => {
  return (
    <div className="top-message">
      <span>Welcome to chatroom</span>
      <br></br>
      <br></br>
      <span className="top-message-second-row">you can</span> <Link to="/rooms"> browse </Link>
      <span className="top-message-second-row">existing rooms</span>
      <br></br>
      <span className="top-message-second-row">or</span> <Link to="/create"> create </Link>
      <span className="top-message-second-row">your own room</span>
    </div>
  );
};

export default HomePage;
