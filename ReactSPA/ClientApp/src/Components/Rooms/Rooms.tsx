import React, { FC, useState, useEffect } from 'react';
import './rooms.css';
import { Page } from './../Shared/Page';
import { Link } from 'react-router-dom';
import { getRooms, RoomData } from '../../Data/RoomDataService';
import { useAuth0 } from '@auth0/auth0-react';
import RoomCard from './RoomCard';

export const Rooms: FC = () => {
  const [rooms, setRooms] = useState<RoomData[] | null>(null);
  const [roomsLoading, setRoomsLoading] = useState(true);

  const { isAuthenticated, getAccessTokenSilently, isLoading } = useAuth0();

  useEffect(() => {
    let cancelled = false;
    const doGetRooms = async () => {
      const token = isAuthenticated ? await getAccessTokenSilently() : '';
      const serverRooms = await getRooms(token);
      if (!cancelled) {
        console.log(serverRooms);
        setRooms(serverRooms);
        setRoomsLoading(false);
      }
    };

    if (!isLoading) {
      doGetRooms();
    }

    return () => {
      cancelled = true;
    };
  }, [getAccessTokenSilently, isAuthenticated, isLoading]);

  return (
    <Page title="Rooms">
      {roomsLoading && (
        <div className="top-message">
          <span>Loading...</span>
        </div>
      )}

      {!roomsLoading && (
        <div className="room-page">
          <Link to="/create">
            <div className="room-card">
              <span className="room-card-create">Create a room</span>
            </div>
          </Link>

          {rooms?.map((room) => (
            <RoomCard key={room.id} {...room}></RoomCard>
          ))}
        </div>
      )}
    </Page>
  );
};
