import React, { FC, useState } from 'react';
import { Page } from './../Shared/Page';
import './rooms.css';
import { useAuth0 } from '@auth0/auth0-react';
import { createRoom } from '../../Data/RoomDataService';
import { Redirect } from 'react-router-dom';

export const CreateRoom: FC = () => {
  const handleSubmit = async (event: any) => {
    event.preventDefault();
    if (roomName.length < 5) {
      alert('Room name should be at least 5 letters');
    } else {
      const token = await getAccessTokenSilently();
      setCreateButtonEnabled(false);
      console.log('user.nickname = ' + user.nickname);
      const postResult = await createRoom(token, {
        name: roomName,
        userNickname: user.nickname,
      });
      setCreateButtonEnabled(true);
      if (!postResult.ok && postResult.error) {
        alert(postResult.error);
      } else {
        if (postResult.data !== undefined) {
          setCreatedRoomId(postResult.data?.id);
          setRedirectToRoom(true);
        }
      }
    }
  };

  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    setRoomName(event.currentTarget.value);
  };

  const { isAuthenticated, getAccessTokenSilently, isLoading, user } = useAuth0();

  const [roomName, setRoomName] = useState('');
  const [createButtonEnabled, setCreateButtonEnabled] = useState(true);

  const [redirectToRoom, setRedirectToRoom] = useState(false);
  const [createdRoomId, setCreatedRoomId] = useState<number>();

  return (
    <Page title="Create room">
      {!isLoading && !isAuthenticated && <Redirect to="/signin-request" />}

      {redirectToRoom && <Redirect to={`/chat/${createdRoomId}`} />}

      <h1>Create a new room:</h1>
      <form onSubmit={handleSubmit} className="create-room-form">
        <label>
          Name:
          <input onChange={handleChange} className="create-room-input" type="text" name="name" />
        </label>
        <input disabled={!createButtonEnabled} type="submit" value="Create" />
      </form>
    </Page>
  );
};
