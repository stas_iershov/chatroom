import React, { FC, useState } from 'react';
import './rooms.css';
import { RoomData, joinToRoom } from '../../Data/RoomDataService';
import { useAuth0 } from '@auth0/auth0-react';
import { Redirect, Link } from 'react-router-dom';

const RoomCard: FC<RoomData> = ({ name, joined, numberOfUsers, id }) => {
  const handleJoinClick = async () => {
    if (!isAuthenticated) {
      setNeedsSigninRedirect(true);
    } else {
      const token = await getAccessTokenSilently();
      console.log('id = ' + id);
      const postResult = await joinToRoom(token, {
        chatRoomId: id,
        userNickname: user.nickname,
      });
      if (!postResult.ok && postResult.error) {
        alert(postResult.error);
      } else {
        setJoinedToRoom(true);
      }
    }
  };

  const { isAuthenticated, getAccessTokenSilently, isLoading, user } = useAuth0();
  const [needsSigninRedirect, setNeedsSigninRedirect] = useState(false);

  const [joinedToRoom, setJoinedToRoom] = useState(joined);

  return (
    <div className="room-card">
      {!isLoading && needsSigninRedirect && <Redirect to="/signin-request" />}

      <strong className="room-card-title">{name}</strong>
      <span className="room-card-members">Members: {numberOfUsers}</span>
      {!joinedToRoom && (
        <button disabled={isLoading} onClick={handleJoinClick} className="room-card-join">
          Join!
        </button>
      )}
      {joinedToRoom && (
        <Link to={`/chat/${id}`}>
          <button className="room-card-join room-card-enter">Enter</button>
        </Link>
      )}
    </div>
  );
};

export default RoomCard;
