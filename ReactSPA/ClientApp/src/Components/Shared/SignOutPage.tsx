import React, { FC } from 'react';
import { Page } from './Page';
import { useAuth0 } from '@auth0/auth0-react';

type SignoutAction = 'signout' | 'signout-callback';
interface Props {
  action: SignoutAction;
}
export const SignOutPage: FC<Props> = ({ action }) => {
  const { logout } = useAuth0();

  let message = 'Signing out ...';

  const logoutWithRedirect = () =>
    logout({
      returnTo: window.location.origin + '/signout-callback',
    });

  if (action === 'signout') {
    logoutWithRedirect();
  } else {
    message = "You've successfully signed out.";
  }

  return (
    <Page title="Sign out">
      <div className="top-message">
        <span>{message}</span>
      </div>
    </Page>
  );
};
