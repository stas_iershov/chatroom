import { FC, useEffect } from 'react';
import React from 'react';

interface Props {
  title?: string;
}

export const Page: FC<Props> = ({ title, children }) => {
  useEffect(() => {
    document.title = title ? 'Chatroom - ' + title : 'Chatroom';
  });

  return <div>{children}</div>;
};
