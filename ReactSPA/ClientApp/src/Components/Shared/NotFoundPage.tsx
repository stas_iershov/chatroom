import React from 'react';
import { Page } from './Page';

export const NotFoundPage = () => (
  <Page title="Page Not Found">
    <h1>404 :(</h1>
  </Page>
);
