import React, { FC, useEffect, useState } from 'react';
import { Page } from './Page';
import { useAuth0 } from '@auth0/auth0-react';
import { Redirect, Link } from 'react-router-dom';

type SigninAction = 'signin' | 'signin-callback' | 'signin-request';
interface Props {
  action: SigninAction;
}

export const SignInPage: FC<Props> = ({ action }) => {
  const { loginWithRedirect } = useAuth0();

  const [navigateHome, setNavigateHome] = useState(false);

  useEffect(() => {
    if (action === 'signin-callback') {
      setNavigateHome(true);
    }
  }, [action]);

  if (action === 'signin') {
    loginWithRedirect();
  }

  return (
    <Page title="Sign In">
      {navigateHome && <Redirect to="/" />}

      {action === 'signin-request' && (
        <div className="top-message">
          <span>Please</span> <Link to="signin"> sign in </Link>
          <span>to continue</span>
        </div>
      )}

      {action === 'signin' && (
        <div className="top-message">
          <span>Signing in ...</span>
        </div>
      )}
    </Page>
  );
};
