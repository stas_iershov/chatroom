import React from 'react';
import './App.css';
import Header from './Components/Header/Header';
import HomePage from './Components/HomePage/HomePage';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import { SignInPage } from './Components/Shared/SignInPage';
import { SignOutPage } from './Components/Shared/SignOutPage';
import { NotFoundPage } from './Components/Shared/NotFoundPage';
import { Rooms } from './Components/Rooms/Rooms';
import { ChatPage } from './Components/ChatPage/ChatPage';
import { Auth0Provider } from '@auth0/auth0-react';
import { authSettings } from './AppSettings';
import { CreateRoom } from './Components/Rooms/CreateRoom';

function App() {
  return (
    <Auth0Provider
      domain={authSettings.domain}
      clientId={authSettings.clientId}
      audience={authSettings.audience}
      redirectUri={authSettings.redirectUri}
    >
      <BrowserRouter>
        <div className="App">
          <Header />
          <Switch>
            <Redirect from="/home" to="/" />
            <Route exact path="/" component={HomePage} />
            <Route path="/rooms" component={Rooms} />
            <Route path="/create" component={CreateRoom} />
            <Route path="/chat/:roomId" component={ChatPage} />
            <Route path="/signin" render={() => <SignInPage action="signin" />} />
            <Route path="/signin-callback" render={() => <SignInPage action="signin-callback" />} />
            <Route path="/signin-request" render={() => <SignInPage action="signin-request" />} />
            <Route path="/signout" render={() => <SignOutPage action="signout" />} />
            <Route path="/signout-callback" render={() => <SignOutPage action="signout-callback" />} />
            <Route component={NotFoundPage} />
          </Switch>
        </div>
      </BrowserRouter>
    </Auth0Provider>
  );
}

export default App;
