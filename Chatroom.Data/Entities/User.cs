﻿using System;
using System.Collections.Generic;
using System.Text;
using Chatroom.Data.Entities.Chat;

namespace Chatroom.Data.Entities
{
    public class User : BaseEFEntity
    {
        public string AuthId { get; set; }
        public virtual ICollection<ChatRoomUser> ChatRoomUsers { get; set; }
        public ICollection<ChatRoom> CreatedRooms { get; set; }
        public string Nickname { get; set; }
    }
}
