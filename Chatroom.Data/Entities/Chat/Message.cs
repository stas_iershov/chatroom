﻿using System;
using Chatroom.Data.Entities;

namespace Chatroom.Data
{
    public class Message
    {
        public string id { get; set; }
        public string MessageText { get; set; }
        public string UserAuthId { get; set; }
        public DateTime Created { get; set; }
        public int RoomId { get; set; }
    }
}
