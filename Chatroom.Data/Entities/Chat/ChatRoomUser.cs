﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.Entities.Chat
{
    public class ChatRoomUser
    {
        public ChatRoom ChatRoom { get; set; }
        public int ChatRoomId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
    }
}
