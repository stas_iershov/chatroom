﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Chatroom.Data.Entities.Chat
{
    public class ChatRoom : BaseEFEntity
    {
        [Required]
        [StringLength(60, MinimumLength = 3)]
        public string Name { get; set; }
        
        //public virtual User Creator { get; set; }
        public int CreatorId { get; set; }
        //public virtual ICollection<User> Users { get; set; }

        public User Creator { get; set; }
        public virtual ICollection<ChatRoomUser> ChatRoomUsers { get; set; }
    }
}
