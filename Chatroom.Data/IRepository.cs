﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data.Entities;

namespace Chatroom.Data
{
    public interface IRepository<T> where T : class
    {
        Task<T> Create(T entity);
        IQueryable<T> All();
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        Task<int> Delete(T entity);
        Task<T> Update(T entity);
        
    }
}
