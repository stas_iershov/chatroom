﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Chatroom.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Chatroom.Data
{
    public class EFRepository<T> : IRepository<T> where T : class
    {
        private readonly ChatroomContext _context;
        protected readonly DbSet<T> _dbSet;

        public EFRepository(ChatroomContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public async Task<T> Create(T entity)
        {
            var result = await _dbSet.AddAsync(entity);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public IQueryable<T> All()
        {
            return _dbSet.AsQueryable();
        }

        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _dbSet.Where(predicate).AsQueryable();
        }

        public async Task<int> Delete(T entity)
        {
            _dbSet.Remove(entity);
            return await _context.SaveChangesAsync();
        }

        public async Task<T> Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return entity;
        }

    }
}
