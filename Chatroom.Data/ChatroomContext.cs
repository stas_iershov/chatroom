﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data.Entities;
using Chatroom.Data.Entities.Chat;
using Microsoft.EntityFrameworkCore;

namespace Chatroom.Data
{
    public class ChatroomContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ChatRoom> ChatRooms { get; set; }
        public DbSet<ChatRoomUser> ChatRoomUser { get; set; }
        //public DbSet<Message> Messages { get; set; }

        public ChatroomContext(DbContextOptions<ChatroomContext> options)
            : base(options)
        { }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ChatRoomUser>().HasKey(x => new {x.UserId, x.ChatRoomId});
            modelBuilder.Entity<User>().HasMany(x => x.CreatedRooms).WithOne(x => x.Creator).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<ChatRoom>().HasOne(x => x.Creator).WithMany(x => x.CreatedRooms);

            modelBuilder.Entity<User>().HasIndex(x => x.AuthId);
        }

    }
}
