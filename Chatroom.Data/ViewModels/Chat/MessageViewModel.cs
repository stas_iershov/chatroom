﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.ViewModels.Chat
{
    public class MessageViewModel
    {
        public string MessageId { get; set; }
        public string MessageText { get; set; }
        public string UserNickname { get; set; }
        public DateTime Created { get; set; }
        public int RoomId { get; set; }
        public string UserAuthId { get; set; }
    }
}
