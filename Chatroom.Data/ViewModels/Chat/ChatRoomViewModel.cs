﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.ViewModels.Chat
{
    public class ChatRoomViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public UserViewModel User { get; set; }
        public int NumberOfUsers { get; set; }
        public bool Joined { get; set; }
    }
}
