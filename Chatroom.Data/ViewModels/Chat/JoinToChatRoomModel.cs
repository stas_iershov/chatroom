﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.ViewModels.Chat
{
    public class JoinToChatRoomModel
    {
        public int ChatRoomId { get; set; }
        public string UserNickname { get; set; }
    }
}
