﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.ViewModels.Chat
{
    public class PostMessageViewModel
    {
        public string MessageText { get; set; }
        public string AuthId { get; set; }
        public int RoomId { get; set; }
    }
}
