﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.ViewModels.Chat
{
    public class CreateChatRoomModel
    {
        public string Name { get; set; }
        public string UserNickname { get; set; }
    }
}
