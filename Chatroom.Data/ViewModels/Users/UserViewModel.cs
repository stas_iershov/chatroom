﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string AuthId { get; set; }
        public string Nickname { get; set; }
    }
}
