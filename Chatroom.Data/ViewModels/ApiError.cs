﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Chatroom.Data.ViewModels
{
    public class ApiError
    {
        public string ErrorMessage { get; set; }
    }
}
