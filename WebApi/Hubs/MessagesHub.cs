﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace WebApi.Hubs
{
    public class MessagesHub : Hub
    {
        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
            await Clients.Caller.SendAsync("Message", "Client connected");
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Clients.Caller.SendAsync("Message", "Client disconnected");
            await base.OnDisconnectedAsync(exception);
        }

        public async Task SubscribeToRoom(int roomId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, $"Room:{roomId}");
            await Clients.Caller.SendAsync("Message", "Client subscribed");
        }

        public async Task UnsubscribeFromRoom(int roomId)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, $"Room:{roomId}");
            await Clients.Caller.SendAsync("Message", "Client unsubscribed");
        }
    }
}
