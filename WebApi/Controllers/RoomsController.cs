﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Chatroom.Data.Entities;
using Chatroom.Data.Entities.Chat;
using Chatroom.Data.ViewModels;
using Chatroom.Data.ViewModels.Chat;
using Chatroom.Services.Users;
using ChatServices.Rooms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomsController : ControllerBase
    {
        private readonly IRoomService _roomService;
        private readonly IUserService _userService;
        private readonly IHttpClientFactory _clientFactory;
        private readonly string _auth0UserInfo;

        public RoomsController(IRoomService roomService, IUserService userService)
        {
            _roomService = roomService;
            _userService = userService;
        }

        //[Authorize]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var isAuthenticated = User.Identity.IsAuthenticated;
                int? userId = null;
                if (isAuthenticated)
                {
                    var user = await _userService.GetUserByAuthId(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                    userId = user.Id;
                }

                var allRooms = await _roomService.GetChatRooms(!isAuthenticated, userId);
                return new JsonResult(allRooms);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiError()
                {
                    ErrorMessage = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post(CreateChatRoomModel roomData)
        {
            try
            {
                var user = await _userService.GetUserByAuthId(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                await _userService.UpdateUserNickname(user.AuthId, roomData.UserNickname);
                var createdRoom = await _roomService.CreateRoom(user, roomData.Name);
                await _roomService.JoinUserToChatRoom(user.Id, createdRoom.Id);
                return new JsonResult(createdRoom);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiError()
                {
                    ErrorMessage = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        [Route("join")]
        public async Task<IActionResult> JoinToRoom(JoinToChatRoomModel model)
        {
            try
            {
                var user = await _userService.GetUserByAuthId(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                await _userService.UpdateUserNickname(user.AuthId, model.UserNickname);
                await _roomService.JoinUserToChatRoom(user.Id, model.ChatRoomId);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiError()
                {
                    ErrorMessage = e.Message
                });
            }
        }

    }
}
