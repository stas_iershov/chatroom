﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Chatroom.Data.ViewModels;
using Chatroom.Data.ViewModels.Chat;
using Chatroom.Services.Messages;
using Chatroom.Services.Users;
using ChatServices.Rooms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using WebApi.Hubs;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMessagesService _messagesService;
        private readonly IRoomService _roomService;
        private readonly IHubContext<MessagesHub> _questionHubContext;

        public MessagesController(IUserService userService, IMessagesService messagesService, IRoomService roomService, IHubContext<MessagesHub> questionHubContext)
        {
            _userService = userService;
            _messagesService = messagesService;
            _roomService = roomService;
            _questionHubContext = questionHubContext;
        }

        [Authorize]
        [HttpGet("{roomId}")]
        [HttpGet]
        public async Task<IActionResult> Get(int roomId)
        {
            try
            {
                var user = await _userService.GetUserByAuthId(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                var isUserInGroup = await CheckIfUserIsInGroup(roomId, user.Id);
                if (!isUserInGroup)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new ApiError()
                    {
                        ErrorMessage = "User does not belong to this group"
                    });
                }

                //TODO: caching here
                var groupUsers = await _roomService.GetRoomUsers(roomId);

                //Dictionary is used for faster lookup:
                var userNames = groupUsers.ToDictionary(x => x.AuthId, x => x.Nickname);

                var messages = await _messagesService.GetMessages(roomId);

                foreach (var messageViewModel in messages)
                {
                    messageViewModel.UserNickname = userNames[messageViewModel.UserAuthId];
                }

                return new JsonResult(messages);


            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiError()
                {
                    ErrorMessage = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Post(PostMessageViewModel model)
        {
            try
            {
                var user = await _userService.GetUserByAuthId(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                var isUserInGroup = await CheckIfUserIsInGroup(model.RoomId, user.Id);
                if (!isUserInGroup)
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, new ApiError()
                    {
                        ErrorMessage = "User does not belong to this group"
                    });
                }

                model.AuthId = user.AuthId;

                var createdModel = await _messagesService.PostMessage(model);
                createdModel.UserNickname = user.Nickname;

                await _questionHubContext.Clients.Group($"Room:{model.RoomId}").SendAsync("MessagePosted", createdModel);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new ApiError()
                {
                    ErrorMessage = e.Message
                });
            }
        }

        private async Task<bool> CheckIfUserIsInGroup(int roomId, int userId)
        {
            var isUserInGroup = await _roomService.IsUserInChatRoom(userId, roomId);
            return isUserInGroup;
        }

    }
}
