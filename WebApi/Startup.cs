using System.Threading.Tasks;
using Chatroom.Data;
using Chatroom.Data.Entities;
using Chatroom.Data.Entities.Chat;
using Chatroom.Services.Cache;
using Chatroom.Services.Messages;
using Chatroom.Services.Users;
using ChatServices.Rooms;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApi.Hubs;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<IMessagesService>(InitializeCosmosClientInstanceAsync(Configuration.GetSection("CosmosDb")).GetAwaiter().GetResult());
            services.AddDbContext<ChatroomContext>(builder => builder.UseSqlServer(Configuration.GetConnectionString("Default")));
            

            services.AddScoped(typeof(IRepository<User>), typeof(EFRepository<User>));
            services.AddScoped(typeof(IRepository<ChatRoom>), typeof(EFRepository<ChatRoom>));
            services.AddScoped(typeof(IRepository<ChatRoomUser>), typeof(EFRepository<ChatRoomUser>));
            
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(IRoomService), typeof(RoomService));
            services.AddSingleton(typeof(IRoomCache), typeof(RoomCache));

            services.AddCors(options => options.AddPolicy("ChatroomCors",
                builder => builder
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .WithOrigins(Configuration["ClientAppStaging"])
                    .WithOrigins(Configuration["ClientApp"])));


            services.AddSignalR();

            services.AddMemoryCache();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme =
                    JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme =
                    JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Authority = Configuration["Auth0:Authority"];
                options.Audience = Configuration["Auth0:Audience"];
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors("ChatroomCors");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<MessagesHub>("/messageshub");
            });
        }

        private static async Task<MessagesService> InitializeCosmosClientInstanceAsync(IConfiguration configurationSection)
        {
            var databaseName = configurationSection.GetSection("CosmosDatabaseName").Value;
            var containerName = configurationSection.GetSection("CosmosContainerName").Value;
            var account = configurationSection.GetSection("CosmosAccount").Value;
            var key = configurationSection.GetSection("CosmosKey").Value;
            var client = new Microsoft.Azure.Cosmos.CosmosClient(account, key);
            var service = new MessagesService(client, databaseName, containerName);
            var database = await client.CreateDatabaseIfNotExistsAsync(databaseName);
            await database.Database.CreateContainerIfNotExistsAsync(containerName, "/RoomId");

            return service;
        }
    }
}
