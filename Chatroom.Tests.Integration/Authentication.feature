﻿Feature: Authentication
	This is the list of requirements
	which describe authentication rules of Chatroom app

@users @auth @rooms
Scenario: User which is not logged in can see the list of rooms

	Given the database contains the following Users:
	| Name      | AuthId |
	| Test User | test   |	
	Given the database contains the following Rooms created by "Test User":
	| Name               | 
	| Test Room          | 	
	Given user is not logged in	
	Then the API returns the following Rooms:
	| Name               | 
	| Test Room          | 	

Scenario: User which is logged in can create a room

	Given the user "Stas" is authenticated against Auth server with the following credentials:
	| Email                               | Password     |
	| chatroom.integration.test@gmail.com | 123qwe!@#QWE |		
	When the user "Stas" creates the following Room:
	| Name               | 
	| Test Room          | 	
	Then the API returns the following Rooms:
	| Name               | 
	| Test Room          | 	


Scenario: User which is not logged in can not create a room

	Given the database contains the following Users:
	| Name      | AuthId |
	| Test User | test   |	
	Given user is not logged in	
	Then the user "Test User" cannot create the following Rooms:
	| Name               | 
	| Test Room          | 	



