﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Chatroom.Data.ViewModels.Chat;
using FluentAssertions;
using Newtonsoft.Json;
using TechTalk.SpecFlow;

namespace Chatroom.Tests.Integration
{
    [Binding]
    public class RoomSteps
    {
        protected readonly ExecutionContext _executionContext;

        public RoomSteps(ExecutionContext executionContext)
        {
            _executionContext = executionContext;
        }


        [When(@"the user ""(.*)"" creates the following Room:")]
        public HttpResponseMessage WhenTheUserCreatesTheFollowingRoom(string userName, Table table)
        {
            var createRoomModel = new CreateChatRoomModel()
            {
                UserNickname = userName,
                Name = table.Rows[0]["Name"]
            };

            var roomData = JsonConvert.SerializeObject(createRoomModel);
            var result = _executionContext.HttpClient.PostAsync("api/rooms", new StringContent(roomData, Encoding.UTF8,
                "application/json")).Result;
            
            return result;
        }

        [Then(@"the user ""(.*)"" cannot create the following Rooms:")]
        public void ThenTheUserCannotCreateTheFollowingRooms(string userName, Table table)
        {
            var createRoomResult = WhenTheUserCreatesTheFollowingRoom(userName, table);
            createRoomResult.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Then(@"the user ""(.*)"" cannot create the following Room with error ""(.*)"":")]
        public void ThenTheUserCannotCreateTheFollowingRoomWithError(string userName, string errorMessage, Table table)
        {
            var createRoomResult = WhenTheUserCreatesTheFollowingRoom(userName, table);
            createRoomResult.StatusCode.Should().Be(HttpStatusCode.InternalServerError);
        }

    }
}
