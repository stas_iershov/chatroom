﻿Feature: Room
	Rooms business logic is
	tested here	

@rooms
Scenario: Users cannot create rooms with the same names
	Given the database contains the following Users:
	| Name      | AuthId |
	| Test User | test   |
	Given the user "Stas" is authenticated against Auth server with the following credentials:
	| Email                               | Password     |
	| chatroom.integration.test@gmail.com | 123qwe!@#QWE |	
	When the user "Stas" creates the following Room:
	| Name               | 
	| Test Room          | 	
	Then the API returns the following Rooms:
	| Name               | 
	| Test Room          | 	
	Then the user "Stas" cannot create the following Room with error "room with the same name already exists":
	| Name               | 
	| Test Room          | 	