﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Chatroom.Data;
using Chatroom.Data.Entities;
using Chatroom.Data.Entities.Chat;
using Chatroom.Data.ViewModels.Chat;
using FluentAssertions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RestSharp;
using TechTalk.SpecFlow;
using Xunit;

namespace Chatroom.Tests.Integration
{

    [Binding]
    public class AuthenticationSteps : StepsBase
    {

        public AuthenticationSteps(ExecutionContext executionContext) : base(executionContext)
        {
        }

        [Given(@"the user ""(.*)"" is authenticated against Auth server with the following credentials:")]
        public void GivenTheUserIsAuthenticatedAgainstAuthWithTheFollowingCredentials(string nickname, Table credInfo)
        {
            var email = credInfo.Rows[0]["Email"];
            var pwd = credInfo.Rows[0]["Password"];

            var client = new RestClient("https://chatroom-demo.eu.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");

            var data = new
            {
                client_id = "VdSJKbRE5QQb2Kcfb8a0sYjjMNMvYkKz",
                client_secret = "eolzaJsjLsAzE2AE57lGeuHv3LcWuVL_j0_rN36EZmZ2LKcOmRqGbmogM-rM27T0",
                audience = "https://chatroom",
                grant_type = "password",
                username = email,
                password = pwd
            };

            
            request.AddParameter("application/json", JsonConvert.SerializeObject(data), ParameterType.RequestBody);
            var response = client.Execute<Auth0TokenData>(request);
            var token = response.Data;


            _executionContext.HttpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            _executionContext.HttpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token.AccessToken);
        }


        [Given(@"the user ""(.*)"" is registered in the system")]
        public void GivenTheUserIsRegisteredInTheSystem(string username)
        {
            _executionContext.Context.Users.Add(new User()
            {
                AuthId = "Test",
                Nickname = "Stas"
            });
            _executionContext.Context.SaveChanges();
        }


        [Given(@"the database contains the following Users:")]
        public void GivenTheDatabaseContainsTheFollowingUsers(Table users)
        {
            foreach (var user in users.Rows)
            {
                _executionContext.Context.Users.Add(new User()
                {
                    AuthId = user["AuthId"],
                    Nickname = user["Name"]
                });
                _executionContext.Context.SaveChanges();
            }
        }

        [Given(@"the database contains the following Rooms created by ""(.*)"":")]
        public void GivenTheDatabaseContainsTheFollowingRoomsCreatedBy(string username, Table rooms)
        {
            var user = _executionContext.Context.Users.First(x => x.Nickname == username);

            foreach (var room in rooms.Rows)
            {
                _executionContext.Context.ChatRooms.Add(new ChatRoom()
                {
                    CreatorId = user.Id,
                    Name = room["Name"]
                });
                _executionContext.Context.SaveChanges();
            }
        }

        [Given(@"user is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            _executionContext.HttpClient.DefaultRequestHeaders.Authorization.Should().Be(null);
        }


        [Then(@"the API returns the following Rooms:")]
        public void ThenTheAPIReturnsTheFollowingRooms(Table rooms)
        {
            var result  = _executionContext.HttpClient.GetStringAsync("api/rooms").Result;
                 
            var data = JsonConvert.DeserializeObject<List<ChatRoomViewModel>>(result);

            foreach (var room in rooms.Rows)
            {
                data.Should().Contain(x => x.Name == room["Name"]);
            }
        }


        [BeforeScenario()]
        public void CleanDb()
        {
            CleanUpDb();
        }

    }

    public class Auth0TokenData
    {
        public string AccessToken { get; set; }
    }

    
}
