﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using Chatroom.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Chatroom.Tests.Integration
{
    public class StepsBase
    {
        protected readonly ExecutionContext _executionContext;

        public StepsBase(ExecutionContext executionContext)
        {
            _executionContext = executionContext;
            SetupHostAndDb();
        }

        protected void SetupHostAndDb()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.test.json")
                .Build();

            var hostBuilder = new HostBuilder()
                .ConfigureWebHost(webHost =>
                {
                    webHost.UseTestServer();
                    webHost.UseConfiguration(config);
                    webHost.UseStartup<WebApi.Startup>();
                });

            var host = hostBuilder.Start();
            var context = host.Services.GetService(typeof(ChatroomContext)) as ChatroomContext;

            _executionContext.Context = context;
            _executionContext.HttpClient = host.GetTestClient();
            _executionContext.Context.Database.EnsureCreated();
        }

        protected void CleanUpDb()
        {
            //_context.Database.EnsureDeleted could also be used (as well as entities)
            //but it would be slower
            _executionContext.Context.Database.ExecuteSqlCommand("delete from dbo.ChatRooms");
            _executionContext.Context.Database.ExecuteSqlCommand("delete from dbo.ChatRoomUser");
            _executionContext.Context.Database.ExecuteSqlCommand("delete from dbo.Users");
            _executionContext.Context.Database.ExecuteSqlCommand("delete from dbo.Messages");
        }
    }

    public class ExecutionContext
    {
        public ChatroomContext Context;
        public HttpClient HttpClient;
    }
}
