﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data.ViewModels.Chat;

namespace Chatroom.Services.Messages
{
    public interface IMessagesService
    {
        Task<List<MessageViewModel>> GetMessages(int roomId);
        Task<MessageViewModel> PostMessage(PostMessageViewModel model);
    }
}
