﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data;
using Chatroom.Data.ViewModels.Chat;
using Microsoft.Azure.Cosmos;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Chatroom.Services.Messages
{
    public class MessagesService : IMessagesService
    {
        private Container _container;
        
        public MessagesService(CosmosClient dbClient,  string databaseName, string containerName)
        {
            _container = dbClient.GetContainer(databaseName, containerName);
        }


        public async Task<List<MessageViewModel>> GetMessages(int roomId)
        {
            var sqlQueryText = $"SELECT * FROM c WHERE c.RoomId = {roomId}";
            var queryDefinition = new QueryDefinition(sqlQueryText);
            var queryResultSetIterator = _container.GetItemQueryIterator<Message>(queryDefinition);
            var messages = new List<Message>();

            while (queryResultSetIterator.HasMoreResults)
            {
                var currentResultSet = await queryResultSetIterator.ReadNextAsync();
                messages.AddRange(currentResultSet);
            }

            return messages.Select(GetMessageViewModel).ToList();
        }

        public async Task<MessageViewModel> PostMessage(PostMessageViewModel model)
        {
            var message = new Message()
            {
                id = Guid.NewGuid().ToString("N"),
                RoomId = model.RoomId,
                Created = DateTime.Now,
                UserAuthId = model.AuthId,
                MessageText = model.MessageText
            };

            await _container.CreateItemAsync<Message>(message, new PartitionKey(model.RoomId));
            return GetMessageViewModel(message);
        }

        private static MessageViewModel GetMessageViewModel(Message message)
        {
            return new MessageViewModel()
            {
                RoomId = message.RoomId,
                MessageId = message.id,
                Created = message.Created,
                MessageText = message.MessageText,
                UserAuthId = message.UserAuthId
            };
        }
    }
}
