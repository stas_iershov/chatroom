﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data;
using Chatroom.Data.Entities;
using Chatroom.Data.Entities.Chat;
using Chatroom.Data.ViewModels;
using Chatroom.Data.ViewModels.Chat;
using Microsoft.EntityFrameworkCore;

namespace Chatroom.Services.Users
{
    public class UserService: IUserService
    {
        private readonly IRepository<User> _repository;

        public UserService(IRepository<User> repository)
        {
            _repository = repository;
        }

   
        public async Task<List<ChatRoomViewModel>> GetCreatedRooms(int userId)
        {
            var user = await _repository.FindBy(x => x.Id == userId).Include(x => x.CreatedRooms).FirstOrDefaultAsync();
            var userVm = GetUserViewModel(user);

            return user.CreatedRooms.Select(x => new ChatRoomViewModel()
            {
                Name = x.Name,
                User = userVm
            }).ToList();
        }

        public async Task UpdateUserNickname(string authId, string nickname)
        {
            var user = await _repository.FindBy(x => x.AuthId == authId).FirstOrDefaultAsync();
            if (user != null)
            {
                user.Nickname = nickname;
                await _repository.Update(user);
            }
        }

        public async Task<UserViewModel> GetUserByAuthId(string authId)
        {
            var user = await _repository.FindBy(x => x.AuthId == authId).FirstOrDefaultAsync();
            if (user == null)
            {
                user = await _repository.Create(new User()
                {
                    AuthId = authId
                });
            }
            return GetUserViewModel(user);
        }

        public static UserViewModel GetUserViewModel(User user)
        {
            return new UserViewModel()
            {
                AuthId = user.AuthId,
                Id = user.Id,
                Nickname = user.Nickname
            };
        }
    }
}
