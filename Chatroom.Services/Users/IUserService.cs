﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data.Entities;
using Chatroom.Data.Entities.Chat;
using Chatroom.Data.ViewModels;
using Chatroom.Data.ViewModels.Chat;

namespace Chatroom.Services.Users
{
    public interface IUserService
    {
        Task<UserViewModel> GetUserByAuthId(string authId);
        Task<List<ChatRoomViewModel>> GetCreatedRooms(int userId);
        Task UpdateUserNickname(string authId, string nickname);
    }
}
