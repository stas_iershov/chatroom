﻿using System;
using System.Collections.Generic;
using System.Text;
using Chatroom.Data.ViewModels;
using Microsoft.Extensions.Caching.Memory;

namespace Chatroom.Services.Cache
{
    public class RoomCache : IRoomCache
    {
        private readonly MemoryCache _cache;
        public RoomCache()
        {
            _cache = new MemoryCache(new MemoryCacheOptions
            {
                SizeLimit = 100
            });
        }

        private string GetCacheKey(int roomId) => $"Room_{roomId}";

        public void Invalidate(int roomId)
        {
            _cache.Remove(GetCacheKey(roomId));
        }

        public void Set(List<UserViewModel> users, int roomId)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSize(1);
            _cache.Set(GetCacheKey(roomId), users, cacheEntryOptions);
        }

        public bool TryGet(int roomId, out List<UserViewModel> users)
        {
            return _cache.TryGetValue(GetCacheKey(roomId), out users);
        }
    }
}
