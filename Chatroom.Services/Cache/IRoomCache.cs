﻿using System;
using System.Collections.Generic;
using System.Text;
using Chatroom.Data.ViewModels;

namespace Chatroom.Services.Cache
{
    public interface IRoomCache
    {
        bool TryGet(int roomId, out List<UserViewModel> users);
        void Set(List<UserViewModel> users, int roomId);
        void Invalidate(int roomId);
    }
}
