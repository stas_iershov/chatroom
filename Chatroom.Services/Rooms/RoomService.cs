﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data;
using Chatroom.Data.Entities;
using Chatroom.Data.Entities.Chat;
using Chatroom.Data.ViewModels;
using Chatroom.Data.ViewModels.Chat;
using Chatroom.Services.Cache;
using Chatroom.Services.Users;
using Microsoft.EntityFrameworkCore;

namespace ChatServices.Rooms
{
    public class RoomService : IRoomService
    {
        private readonly IRepository<ChatRoom> _repository;
        private readonly IRepository<ChatRoomUser> _chatRoomUserRepository;
        private readonly IRoomCache _roomCache;

        public RoomService(IRepository<ChatRoom> repository, IRepository<ChatRoomUser> chatRoomUserRepository, IRoomCache roomCache)
        {
            _repository = repository;
            _chatRoomUserRepository = chatRoomUserRepository;
            _roomCache = roomCache;
        }

        public async Task<List<ChatRoomViewModel>> GetChatRooms(bool anonymously, int? userId)
        {
            var allRooms = await _repository.All().Include(x => x.ChatRoomUsers).Select(x => new ChatRoomViewModel()
            {
                Id = x.Id,
                NumberOfUsers = x.ChatRoomUsers.Count,
                Name = x.Name,
                Joined = !anonymously && (userId.HasValue && x.ChatRoomUsers.Any(user => user.UserId == userId.Value))
            }).ToListAsync();
            
            return allRooms;
        }

        public async Task JoinUserToChatRoom(int userId, int roomId)
        {
            _roomCache.Invalidate(roomId);

            var existingConnection = await _chatRoomUserRepository.FindBy(x => x.UserId == userId && x.ChatRoomId == roomId).FirstOrDefaultAsync();
            if (existingConnection == null)
            {
                var newConnection = new ChatRoomUser()
                {
                    ChatRoomId = roomId,
                    UserId = userId
                };
                await _chatRoomUserRepository.Create(newConnection);
            }
        }

        public async Task<bool> IsUserInChatRoom(int userId, int roomId)
        {
            if (_roomCache.TryGet(roomId, out var users))
            {
                return users.Any(x => x.Id == userId);
            }

            var existingConnection = await _chatRoomUserRepository.FindBy(x => x.UserId == userId && x.ChatRoomId == roomId).FirstOrDefaultAsync();
            return existingConnection != null;
        }

        public async Task<List<UserViewModel>> GetRoomUsers(int roomId)
        {
            if (_roomCache.TryGet(roomId, out var users))
            {
                return users;
            }

            var roomUsers = await _chatRoomUserRepository.All().Where(x => x.ChatRoomId == roomId)
                .Select(u => UserService.GetUserViewModel(u.User)).ToListAsync();
            _roomCache.Set(roomUsers, roomId);
            return roomUsers;

        }

        public async Task<ChatRoomViewModel> CreateRoom(UserViewModel user, string roomName)
        {
            var existingRoom = await _repository.FindBy(x => x.Name.ToLower().Trim() == roomName.ToLower().Trim()).FirstOrDefaultAsync();
            if (existingRoom != null)
            {
                throw new InvalidOperationException(
                    $"Could not create a room {roomName}, room with the same name already exists.");
            }

            var chatRoom = await _repository.Create(new ChatRoom()
            {
                CreatorId = user.Id,
                Name = roomName
            });

            return GetChatRoomViewModel(chatRoom);
        }

        public static ChatRoomViewModel GetChatRoomViewModel(ChatRoom chatRoom)
        {
            return new ChatRoomViewModel()
            {
                Id = chatRoom.Id,
                Name = chatRoom.Name
            };
        }

    }
}
