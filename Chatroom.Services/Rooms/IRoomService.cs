﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Chatroom.Data.Entities;
using Chatroom.Data.ViewModels;
using Chatroom.Data.ViewModels.Chat;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace ChatServices.Rooms
{
    public interface IRoomService
    {
        Task<ChatRoomViewModel> CreateRoom(UserViewModel user, string roomName);
        Task<List<ChatRoomViewModel>> GetChatRooms(bool anonymously, int? userId);
        Task JoinUserToChatRoom(int userId, int roomId);
        Task<bool> IsUserInChatRoom(int userId, int roomId);
        Task<List<UserViewModel>> GetRoomUsers(int roomId);
    }
}
